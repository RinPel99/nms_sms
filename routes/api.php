<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AllocationController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/forgot-password', function () {
    return inertia('Auth/forgot-password');
  })->middleware('guest')->name('password.request');
  
  Route::post('/forgot-password', function (Request $request) {
      $request->validate(['email' => 'required|email']);
   
      $status = Password::sendResetLink(
          $request->only('email')
      );
   
      return $status === Password::RESET_LINK_SENT
                  ? back()->with(['status' => __($status)])
                  : back()->withErrors(['email' => __($status)]);
  })->middleware('guest')->name('password.email');
  
  Route::get('/reset-password/{token}', function (string $token) {
    return inertia('Auth/reset-password', ['token' => $token]);
  })->middleware('guest')->name('password.reset');
   
  Route::post('/reset-password', function (Request $request) {
      $request->validate([
          'token' => 'required',
          'email' => 'required|email',
          'password' => 'required|min:8|confirmed',
      ]);
   
      $status = Password::reset(
          $request->only('email', 'password', 'password_confirmation', 'token'),
          function (User $user, string $password) {
              $user->forceFill([
                  'password' => Hash::make($password)
              ])->setRememberToken(Str::random(60));
   
              $user->save();
   
              event(new PasswordReset($user));
          }
      );
   
      return $status === Password::PASSWORD_RESET
                  ? redirect()->route('login')->with('status', __($status))
                  : back()->withErrors(['email' => [__($status)]]);
  })->middleware('guest')->name('password.update');
  
  
  