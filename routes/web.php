<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HospitalController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\UserController;
use App\Models\Doctor;
use App\Models\Events;
use App\Models\Hospital;
use App\Models\Leave;
use App\Models\Training;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Inertia\Inertia;


Route::get('/forgot-password', [IndexController::class, 'forgotpass']);
Route::get('/login', [AuthController::class, 'create'])->name('login');
Route::post('/login', [AuthController::class, 'store']);
Route::delete('/logout', [AuthController::class, 'destroy']);

Route::get('/admin/home', [IndexController::class, 'getAdminHome']);
Route::post('/addHospital', [HospitalController::class, 'addHospital']);
Route::post('/deleteHospital', [HospitalController::class, 'deleteHospital']);
Route::post('/addDirector', [UserController::class, 'addDirector']);
Route::post('/editHospitalDetails', [HospitalController::class, 'editDetails']);
Route::delete('/deleteUser/{userID}', [UserController::class, 'deleteUsers']);

Route::get('/adm/home', [IndexController::class, 'getADMDashboard']);
Route::get('/adm/individualStat', [IndexController::class, 'getADMIndividualStat'])->name('getADMIndividualStat');
Route::post('/doctorAdd', [DoctorController::class, 'addDoctor']);
Route::post('/editDoctor', [DoctorController::class, 'editDoctor']);
Route::post('/deleteDoc', [DoctorController::class, 'deleteDoc']);
Route::post('/transferDoc', [DoctorController::class, 'transferDoc']);
Route::post('/setActive',[DoctorController::class, 'setActive']);
Route::post('/setLeave',[DoctorController::class, 'setLeave']);
Route::post('/setTraining',[DoctorController::class, 'setTraining']);
Route::post('/setEvents',[DoctorController::class, 'setEvents']);

Route::get('/adm/docProfile/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('ADM/docPage', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'events' => Events::all(),
      'leave' => Leave::all(),
      'training' => Training::all(),
  ]);
  } else {
    return redirect('/login');
  }
})->name('doctorProfile');

Route::get('/adm/leaveHystory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('ADM/leaveHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'leave' => Leave::all(),
  ]);
  } else {
    return redirect('/login');
  }
});

Route::get('/adm/TrainingHystory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('ADM/trainingHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'training' => Training::all(),
  ]);
  } else {
    return redirect('/login');
  }
})->name('doctorProfile');

Route::get('/adm/eventHistory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('ADM/eventHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'events' => Events::all(),
  ]);
  } else {
    return redirect('/login');
  }
})->name('doctorProfile');

Route::get('/director/home', [IndexController::class, 'getDirectorDashboard']);
Route::get('/director/individualStat', [IndexController::class, 'getDirectorIndividualStat']);
Route::get('/director/docProfile/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('Director/docPage', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'events' => Events::all(),
      'leave' => Leave::all(),
      'training' => Training::all(),
  ]);
  } else {
    return redirect('/login');
  }
})->name('doctorProfile');

Route::get('/director/leaveHystory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('Director/leaveHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'leave' => Leave::all(),
  ]);
  } else {
    return redirect('/login');
  }
});

Route::get('/director/TrainingHystory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('Director/trainingHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'training' => Training::all(),
  ]);
  } else {
    return redirect('/login');
  }
})->name('doctorProfile');

Route::get('/director/eventHistory/{id}/', function ($id) {
  $user = Auth::user();
  if ($user) {
    return Inertia::render('Director/eventHystory', [
      'id' => $id,
      'doctor' => Doctor::all(),
      'hospital' => Hospital::all(),
      'user' => auth()->user(),
      'events' => Events::all(),
  ]);
  } else {
    return redirect('/login');
  }
  
})->name('doctorProfile');

Route::prefix('adm')
    ->name('adm.')
    ->middleware('auth', 'user_type:ADM')
    ->group(function () {
        Route::get('/adm/home', [IndexController::class, 'getADMDashboard'])->name('ADMHome');

    });

Route::prefix('admin')
    ->name('admin.')
    ->middleware('auth', 'user_type:Admin')
    ->group(function () {
        Route::get('/admin/home', [indexController::class, 'getAdminHome'])->name('AdminHome');

    });

Route::prefix('director')
    ->name('director.')
    ->middleware('auth', 'user_type:Director')
    ->group(function () {
        Route::get('/director/home', [IndexController::class, 'getDirectorHome'])->name('DirectorHome');
 
    });

    Route::get('/forgot-password', function () {
      return inertia('Auth/forgot-password');
    })->middleware('guest')->name('password.request');
    
    Route::post('/forgot-password', function (Request $request) {
        $request->validate(['email' => 'required|email']);
     
        $status = Password::sendResetLink(
            $request->only('email')
        );
     
        return $status === Password::RESET_LINK_SENT
                    ? back()->with(['status' => __($status)])
                    : back()->withErrors(['email' => __($status)]);
    })->middleware('guest')->name('password.email');
    
    Route::get('/reset-password/{token}&{email}', function (string $token, $email) {
      return inertia('Auth/reset-password', ['token' => $token, 'email' => $email]);
    })->middleware('guest')->name('password.reset');
     
    Route::post('/reset-password', function (Request $request) {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);
     
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
     
                $user->save();
     
                event(new PasswordReset($user));
            }
        );
     
        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('login')->with('status', __($status))
                    : back()->withErrors(['email' => [__($status)]]);
    })->middleware('guest')->name('password.update');
    


Route::get('/', function () {
  $user = Auth::user();
  if ($user) {
    if ($user->user_type === 'Admin') {
      return redirect('/admin/home');
    } else if ($user->user_type === 'ADM') {
      return redirect('/adm/home');
    } else if ($user->user_type === 'Director') {
      return redirect('/director/home');
    }
  } else {
    return redirect('/login');
  }
});
