<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('DoctorName');
            $table->string('EmployeeID')->unique();
            $table->string('profile_picture')->nullable();
            $table->string('Education');
            $table->string('Title');
            $table->string('Department');
            $table->string('phone');
            $table->string('email')->unique();
            $table->foreignId('hospital_id');
            $table->string('hospital_name');
            $table->string('current_status');
            $table->string('current_remarks')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('doctors');
    }
};
