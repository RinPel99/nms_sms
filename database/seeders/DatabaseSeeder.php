<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Hospital;
use App\Models\User;
use App\Models\WeeklyHours;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'username' => 'Admin',
            'email' => 'admin@gmail.com',
            'user_type' => 'Admin',
            'password' => 'password'
        ]);
        User::factory()->create([
            'username' => 'ADM',
            'email' => 'adm@gmail.com',
            'user_type' => 'ADM',
            'password' => 'password'
        ]);
        User::factory()->create([
            'username' => 'Director',
            'email' => 'director@gmail.com',
            'user_type' => 'Director',
            'password' => 'password'
        ]);
        Hospital::create([
            'name' => 'JDWNRH',
            'ADM_Name' => 'ADM',
            'ADMEmail' => 'adm@gmail.com',
            'Location' => 'Thimphu',
        ]);

    }
}
