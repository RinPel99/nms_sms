<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $fillable = ['startDate', 'endDate', 'Remarks', 'Attachments', 'doctor_id','hospital_id'];
    protected $appends = ['src'];

    public function getSrcAttribute(){
        return asset("storage/{$this->Attachments}");
    }
}
