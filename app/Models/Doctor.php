<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['DoctorName','profile_picture','EmployeeID', 'Education', 'Title','Department','current_remarks', 'phone', 'email', 'hospital_id', 'hospital_name', 'current_status'];
    protected $appends = ['src'];

    public function getSrcAttribute(){
        return asset("storage/{$this->profile_picture}");
    }
}
