<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Events;
use App\Models\Hospital;
use App\Models\Leave;
use App\Models\Training;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getAdminHome(){
        return inertia('Admin/home', [
            'doctor' => Doctor::all(),
            'hospital' => Hospital::all(),
            'user' => auth()->user(),
            'users' => User::all(),
        ]);
    }
    public function getADMDashboard(){

        return inertia('ADM/dashboard', [
            'doctor' => Doctor::all(),
            'hospital' => Hospital::all(),
            'user' => auth()->user(),
            'events' => Events::all(),
            'leave' => Leave::all(),
            'training' => Training::all(),
        ]);
    }
    public function getADMIndividualStat(){

        return inertia('ADM/individualStat', [
            'doctor' => Doctor::all(),
            'hospital' => Hospital::all(),
            'user' => auth()->user(),
        ]);
    }

    public function getDirectorDashboard(){

        return inertia('Director/dashboard', [
            'doctor' => Doctor::all(),
            'hospital' => Hospital::all(),
            'user' => auth()->user(),
            'events' => Events::all(),
            'leave' => Leave::all(),
            'training' => Training::all(),
        ]);
    }
    public function getDirectorIndividualStat(){

        return inertia('Director/individualStat', [
            'doctor' => Doctor::all(),
            'hospital' => Hospital::all(),
            'user' => auth()->user(),
        ]);
    }
   

}
