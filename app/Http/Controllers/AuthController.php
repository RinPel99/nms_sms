<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function Laravel\Prompts\alert;

class AuthController extends Controller
{
    public function create(){
        return inertia(
            'Auth/Create'
        );
    }
    public function forgotpass(){
        return inertia(
            'Auth/forgot-password'
        );
    }

    public function store(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
            'user_type' => 'required|string|in:ADM,Admin,Director', 
        ]);
        
        // Attempt to authenticate the user
        if (Auth::attempt($credentials)) {
            $user = User::where('email', $credentials['email'])->first();
            // Check if the user's type matches the provided userType
            if ($user->user_type == $credentials['user_type']) {
                // User type matches, return userType in the response
                    $request->session()->regenerate();
                    // dd($credentials, $request);
                    return redirect()->intended('/');
            } else {
                // User type doesn't match, logout and return error response
                return back()->withErrors(['user_type' => 'User type mismatch.']);
                Auth::logout();
            }
        } else {
            return back()->withErrors(['credientals' => 'Invalid credentials.']);
        }
    }


    public function destroy(Request $request){
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
