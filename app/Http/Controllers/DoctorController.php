<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Events;
use App\Models\Hospital;
use App\Models\Leave;
use App\Models\Training;
use Barryvdh\Reflection\DocBlock\Location;
use Illuminate\Contracts\Support\ValidatedData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DoctorController extends Controller
{
    public function addDoctor(Request $request)
    {
        
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:doctors,email',
            'highestFormEdu' => 'required',
            'title' => 'required',
            'department' => 'required',
            'phoneNo' => ['required', 'regex:/^(77|17)\d{6}$/'],
            'employeeID' => 'required',
            'hoepitalID' => 'required',
            'profile_picture' => 'required|image',
        ]);

        $profilePicturePath = $request->file('profile_picture')->store('profile_pictures', 'public');
        
        $hos = Hospital::find($validatedData['hoepitalID']);

        Doctor::create([
            'DoctorName' => $validatedData['name'],
            'email' => $validatedData['email'],
            'Education' => $validatedData['highestFormEdu'],
            'Title' => $validatedData['title'],
            'Department' => $validatedData['department'],
            'phone' => $validatedData['phoneNo'],
            'EmployeeID' => $validatedData['employeeID'],
            'hospital_id' => $validatedData['hoepitalID'],
            'hospital_name' => $hos['name'],
            'current_status' => 'Active',
            'profile_picture' => $profilePicturePath,
        ]);
        return redirect()->back();
    }
    public function editDoctor(Request $request)
    {

        $doctor = Doctor::findOrFail($request->input('id'));

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:doctors,email,' . $doctor->id,
            'highestFormEdu' => 'required',
            'title' => 'required',
            'department' => 'required',
            'phoneNo' => ['required', 'regex:/^(77|17)\d{6}$/'],
            'profile_picture' => 'nullable|image',
        ]);

        $doctor->DoctorName = $validatedData['name'];
        $doctor->email = $validatedData['email'];
        $doctor->Education = $validatedData['highestFormEdu'];
        $doctor->Title = $validatedData['title'];
        $doctor->Department = $validatedData['department'];
        $doctor->phone = $validatedData['phoneNo'];

        if ($request->hasFile('profile_picture')) {
            Storage::delete('public/' . $doctor->profile_picture);

            $profilePicturePath = $request->file('profile_picture')->store('profile_pictures', 'public');
            $doctor->profile_picture = $profilePicturePath;
        }

        $doctor->save();
        return redirect()->back()->with(['reload' => 'true']);
    }
    public function deleteDoc(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
        ]);

        $doc = Doctor::find($request->input('id'));

        Storage::delete('public/' . $doc->profile_picture);
        $leave = Leave::all();
        foreach ($leave as $record) {
            if ($record->doctor_id == $request->input('id')) {
                $record->delete();
            }
        }
        $train = Training::all();
        foreach ($train as $record) {
            if ($record->doctor_id == $request->input('id')) {
                $record->delete();
            }
        }
        $event = Events::all();
        foreach ($event as $record) {
            if ($record->doctor_id == $request->input('id')) {
                $record->delete();
            }
        }

        $doc->delete();
        return redirect()->route('getADMIndividualStat')->with('reload', true);;
        
    }
    public function transferDoc(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
            'hospital' => 'required',
        ]);

        $doc = Doctor::find($request->input('id'));
        $hos = Hospital::find($request->input('hospital'));
        $doc->update([
            'hospital_id' => $request->input('hospital'),
            'hospital_name' => $hos['name']
        ]);
        return redirect()->route('getADMIndividualStat')->with('reload', true);;
        
    }
    public function setActive(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
        ]);

        $doc = Doctor::find($request->input('id'));
        $remark = $request->input('remark') ?? ' '; 

        $doc->update([
            'current_status' => 'Active',
            'current_remarks' => $remark
        ]);
        
        return redirect()->back()->with('reload', true);
    }

    public function setLeave(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
            'start' => 'required',
            'remark' => 'required',
            'end' => 'required',
            'doc' => 'required'
        ]);

        $doc = Doctor::find($request->input('id'));

        $doc->update([
            'current_status' => 'Leave',
            'current_remarks' => $request->input('remark')
        ]);

        $docPath = $request->file('doc')->store('Additional_Documents', 'public');

        Leave::create([
            'doctor_id' => $request->input('id'),
            'hospital_id' => $doc->hospital_id,
            'startDate' => $request->input('start'),
            'endDate' => $request->input('end'),
            'Remarks' => $request->input('remark'),
            'Attachments' => $docPath,
        ]);
        return redirect()->back()->with('reload', true);
    }
    public function setTraining(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
            'start' => 'required',
            'remark' => 'required',
            'end' => 'required',
            'doc' => 'required'
        ]);

        $doc = Doctor::find($request->input('id'));

        $doc->update([
            'current_status' => 'Training',
            'current_remarks' => $request->input('remark')
        ]);

        $docPath = $request->file('doc')->store('Additional_Documents', 'public');

        Training::create([
            'doctor_id' => $request->input('id'),
            'hospital_id' => $doc->hospital_id,
            'startDate' => $request->input('start'),
            'endDate' => $request->input('end'),
            'Remarks' => $request->input('remark'),
            'Attachments' => $docPath,
        ]);
        return redirect()->back()->with('reload', true);
    }
    public function setEvents(Request $request)
    {
        
        $request->validate([
            'id' => 'required',
            'start' => 'required',
            'remark' => 'required',
            'end' => 'required',
            'doc' => 'required'
        ]);

        $doc = Doctor::find($request->input('id'));

        $doc->update([
            'current_status' => 'Events',
            'current_remarks' => $request->input('remark')
        ]);

        $docPath = $request->file('doc')->store('Additional_Documents', 'public');

        Events::create([
            'doctor_id' => $request->input('id'),
            'hospital_id' => $doc->hospital_id,
            'startDate' => $request->input('start'),
            'endDate' => $request->input('end'),
            'Remarks' => $request->input('remark'),
            'Attachments' => $docPath,
        ]);
        return redirect()->back()->with('reload', true);
    }
}
