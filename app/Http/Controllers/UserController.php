<?php

namespace App\Http\Controllers;

use App\Models\User;
use Barryvdh\LaravelIdeHelper\UsesResolver;
use Hash;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Mail;
use Str;

class UserController extends Controller
{
    public function addDirector(Request $request)
    {
        $request->validate([
                'name' => 'required',
                'email' => 'required|unique:users,email',
        ]);
        $randomPassword = Str::random(8);

        $data['username'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['password'] = Hash::make($randomPassword); // Hash the password

        $data['user_type'] = 'Director';

        $user = User::factory()->create($data);

        $this->sendPasswordEmail($data['email'], $randomPassword);
        return redirect()->back();
        
    }
    private function sendPasswordEmail($email, $password)
    {
        // You can customize the email view and subject as needed
        Mail::send('emails.newUserPassword', ['password' => $password], function ($message) use ($email) {
            $message->to($email)->subject('Your New User Password');
        });
    }
    public function deleteUsers($id)
        {
            $item = User::find($id);

            $item->delete();

            return redirect()->back();
        }
        

        public function editUser(Request $request, User $user)
        {

            $this->validate($request, [
                'name' => 'required|string',
            ]);

            // Update the user's information
            $user->update([
                'name' => $request->input('name'),
            ]);

            return redirect()->back();

        }
         
}
