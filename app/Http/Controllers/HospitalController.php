<?php

namespace App\Http\Controllers;

use App\Models\Events;
use App\Models\Hospital;
use App\Models\Leave;
use App\Models\Training;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use Mail;
use Str;

class HospitalController extends Controller
{
    public function addHospital(Request $request) 
    {
    // Validate the request data
    $request->validate([
        'h_name' => 'required',
        'adm_name' => 'required',
        'adm_email' => 'required|unique:hospitals,ADMemail|unique:users,email',
        'location' => 'required',
    ]);

    // Create a new class
    Hospital::create([
        'name' => $request->input('h_name'),
        'ADM_Name' => $request->input('adm_name'),
        'ADMEmail' => $request->input('adm_email'),
        'Location' => $request->input('location'),
    ]);

        $randomPassword = Str::random(8);

        $data['username'] = $request->input('adm_name');
        $data['email'] = $request->input('adm_email');
        // Add the generated password to the data array
        $data['password'] = Hash::make($randomPassword); // Hash the password

        $data['user_type'] = 'ADM';

        User::factory()->create($data);

        $this->sendPasswordEmail($data['email'], $randomPassword, $request->input('h_name'));
        return redirect()->back();
    }

    private function sendPasswordEmail($email, $password, $hosp)
    {
        // You can customize the email view and subject as needed
        Mail::send('emails.newUserPasswordADM', ['password' => $password, 'hosp' => $hosp], function ($message) use ($email) {
            $message->to($email)->subject('Appointment as ADM');
        });
    }
    public function deleteHospital(Request $request) 
    {
    // Validate the request data
    $request->validate([
        'id' => 'required',
    ]);
    $hospital = Hospital::findOrFail($request->input('id'));

    $user = User::all();

    foreach ($user as $use) {
        if ($use->email == $hospital->ADMEmail) {
            $use->delete();
        };
    };

    Mail::send('emails.removedAsADM', ['hosp' => $hospital['name']], function ($message) use ($hospital) {
        $message->to($hospital->ADMEmail)->subject('Removed as ADM');
    });
    $leave = Leave::all();
        foreach ($leave as $record) {
            if ($record->hospital_id == $request->input('id')) {
                $record->delete();
            }
        }
        $train = Training::all();
        foreach ($train as $record) {
            if ($record->hospital_id == $request->input('id')) {
                $record->delete();
            }
        }
        $event = Events::all();
        foreach ($event as $record) {
            if ($record->hospital_id == $request->input('id')) {
                $record->delete();
            }
        }
    $hospital->delete();

    return redirect()->back()->with('success', 'Hospital deleted successfully');
    }

    public function editDetails(Request $request) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:hospitals,ADMemail|unique:users,email',
        ]);

        $data['username'] = $request->input('name');
        $data['email'] = $request->input('email');

        $hospital = Hospital::find($request->input('id'));
        $oldADM = User::where('email', $hospital['ADMEmail'])->first();

        $randomPassword = Str::random(8);

        $data['password'] = Hash::make($randomPassword); // Hash the password

        $data['user_type'] = 'ADM';

        User::factory()->create($data);

        $hospital->update(
            [
                'ADM_Name' => $request->input('name'),
                'ADMEmail' => $request->input('email'),
            ]
        );

        $this->sendPasswordEmail($data['email'], $randomPassword, $hospital['name']);

        if ($oldADM) {
            Mail::send('emails.removedAsADM', ['hosp' => $hospital['name']], function ($message) use ($oldADM) {
                $message->to($oldADM['email'])->subject('Removed as ADM');
            });
    
            $oldADM->delete();
        }
        return redirect()->back();
    }
    


}
