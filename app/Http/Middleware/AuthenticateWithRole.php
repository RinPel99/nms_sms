<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthenticateWithRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array  ...$userTypes
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request, Closure $next, ...$userTypes)
    {
        if (!$request->user() || !in_array($request->user()->user_type, $userTypes)) {
            abort(Response::HTTP_FORBIDDEN, 'Unauthorized');
        }

        return $next($request);
    }
}